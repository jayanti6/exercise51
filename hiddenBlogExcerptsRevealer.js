function ExcerptRevealer(domElements){
  this.headings = domElements.headings;
  this.paragraphs = domElements.paragraphs;
}

ExcerptRevealer.prototype.bindClickToHeadings = function(){
  var _this = this;
  this.headings.on("click", function(event){
    var excerpt = $(this).next();
    event.preventDefault();
    excerpt.slideDown();
    _this.paragraphs.not(excerpt).slideUp();
  });
};

ExcerptRevealer.prototype.init = function(){
  this.bindClickToHeadings();
}

$(document).ready(function(){
  var blog = $("div#blog");
  var domElements = {
    headings: blog.find("h3"),
    paragraphs: blog.find("p.excerpt")
  };
  var excerptRevealer = new ExcerptRevealer(domElements);
  excerptRevealer.init();
});